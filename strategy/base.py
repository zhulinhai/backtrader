from __future__ import (absolute_import, division, print_function, unicode_literals)

import datetime # For datetime objects
import os.path  # To manage paths
import sys # To find out the script name (in argv[0])

# append module root directory to sys.path
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# Import the backtrader platform
import backtrader as bt

# Create a Strategy
class TestStrategy(bt.Strategy):
  params = (
    ('maperiod', 20),
    ('printlog', False),
    ('exitbars', 5),
  )

  def log(self, txt, dt=None, doprint=False):
    ''' Logging function for this strategy '''
    if self.params.printlog or doprint:
      dt = dt or self.datas[0].datetime.date(0)
      print('%s, %s' % (dt.isoformat(), txt))

  def __init__(self):
    # Keep a reference to the "close" line in the data[0] dataseries
    self.dataclose = self.datas[0].close

    # To keep track of pending orders and buy price/commission
    self.order = None
    self.buyprice = None
    self.buycomm = None

    # Add a MovingAverageSimple indicator
    self.sma = bt.indicators.MovingAverageSimple(self.datas[0], period=self.params.maperiod)

    # Indicators for the plotting show
    bt.indicators.ExponentialMovingAverage(self.datas[0], period=25)
    bt.indicators.WeightedMovingAverage(self.datas[0], period=25).subplot = True
    bt.indicators.StochasticSlow(self.datas[0])
    bt.indicators.MACDHisto(self.datas[0])
    rsi = bt.indicators.RSI(self.datas[0])
    bt.indicators.SmoothedMovingAverage(rsi, period=10)
    bt.indicators.ATR(self.datas[0]).plot = False

  def notify_order(self, order):
    if order.status in [order.Submitted, order.Accepted]:
      # Buy/Sell order submitted/accepted to/by broker - Nothing to do
      return
    # Check if an order has been completed
    # Attention: broker could reject order if not enough cash
    if order.status in [order.Completed]:
      if order.isbuy():
        self.log(
          'BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' % 
          (order.executed.price,
          order.executed.value,
          order.executed.comm))
        
        self.buyprice = order.executed.price
        self.buycomm = order.executed.comm
        
      else: # Sell
        self.log('SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
          (order.executed.price,
          order.executed.value,
          order.executed.comm))

      self.bar_executed = len(self)

    elif order.status in [order.Canceled, order.Margin, order.Rejected]:
      self.log('Order Canceled/Margin/Rejected')
    
    # Write down: no pending order
    self.order = None

  def notify_trade(self, trade):
    if not trade.isclosed:
      return

    self.log('OPERATION PROFIT, GROSS %.2f, NET %.2f' %
            (trade.pnl, trade.pnlcomm))

  def next(self):
    # Simply log the closing price of the series from the reference
    self.log('Close, %.2f' % self.dataclose[0])

    # Check if an order is pending ... if yes, we cannot send a 2nd one
    if self.order:
      return
    
    # Check if we are in market
    if not self.position:

      # Not yet ... we MIGHT BUY if ...

      # if self.dataclose[0] < self.dataclose[-1]:
      #   # current close less than previous close

      #   if self.dataclose[-1] < self.dataclose[-2]:
      if self.dataclose[0] > self.sma[0]:
          # previous close less than the previous close

          # BUY BUY BUY!!! (with all possible default parameters)
          self.log('BUY CREATE, %.2f' % self.dataclose[0])
          
          # Keep track of the created order to avoid a 2nd order
          self.order = self.buy()
    
    else:

      # Alredy in the market ... we might sell
      # if len(self) >= (self.bar_executed + self.params.exitbars):
      if self.dataclose[0] < self.sma[0]:
        # SELL, SELL, SELL!!! (with all possible default parameters)
        self.log('SELL CREATE, %.2f' % self.dataclose[0])

        # Keep track of the created order to avoid a 2nd order
        self.order = self.sell()

  def stop(self):
    self.log('(MA Period %2d) Ending Value %.2f' % (self.params.maperiod, self.broker.getvalue()), doprint=True)

if __name__ == '__main__':
  # Create a cerebro entity
  cerebro = bt.Cerebro()

  # Add a strategy
  # cerebro.addstrategy(TestStrategy)

  # Add a strategy
  strats = cerebro.optstrategy(
    TestStrategy,
    maperiod=range(3, 31))


  modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
  # datapath = os.path.join(modpath, '../datas/orcl-1995-2014.txt')
  datapath = os.path.join(modpath, '../datas/XPEV.csv')

  # Create a Data Feed
  data = bt.feeds.YahooFinanceCSVData(
    dataname=datapath,
    # Do not pass values before the date
    fromdate=datetime.datetime(2020,8,27),
    # Do not pass values after this date
    todate=datetime.datetime(2020,12,31),
    reversed=False
  )

  # Add the Data Feed to Cerebro
  cerebro.adddata(data)
  
  # Set our desired cash start
  cerebro.broker.setcash(1000.0)

  # Add a FixedSize sizer according to the stake
  cerebro.addsizer(bt.sizers.FixedSize, stake=10)

  # 0.1% ... divide by 100 to remove the %
  cerebro.broker.setcommission(commission=0.0)

  print('Starting Profolio Value: %2.f' % cerebro.broker.getvalue())

  # Run over everything
  cerebro.run(maxcpus=1)

  print('Final Profolio Value: %.2f' % cerebro.broker.getvalue())

  # Plot the result
  cerebro.plot()