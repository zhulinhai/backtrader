from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import datetime  # 用于datetime对象操作
import os.path  # 用于管理路径
import sys  # 用于在argvTo[0]中找到脚本名称

# append module root directory to sys.path
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import backtrader as bt  # 引入backtrader框架
import pandas as pd
import backtest

stk_num = 1  # 回测股票数目

# 创建策略
class Bolling(bt.Strategy):
    # 可配置策略参数
    params = dict(
        period = 20,
        period_volume=10,   # 前n日最大交易量
        p_sell_ma=5,          # 跌破该均线卖出
        p_oneplot=False,      # 是否打印到同一张图
        pstake=1000,          # 单笔交易股票数
    )
    
    def __init__(self):
        self.bb = bt.ind.BBands(period = self.p.period, devfactor = 2.0)
        self.dataclose = self.datas[0].close
        self.order = None
        
    def log(self, txt, dt=None):
        dt = dt or self.datas[0].datetime.date(0)
        print('%s, %s' % (dt.isoformat(), txt))
        
    def next(self):
        # self.log("持仓%d" % (self.position.size))
        if self.order:
            return
        if not self.position:
            # 突破下轨 买入
            if self.bb.bot > self.datas[0].close:
                cash = self.broker.get_cash()
                stock = math.ceil(cash/self.dataclose/100)*100 - 100
                self.order = self.buy(size = stock, price = self.datas[0].close, exectype = bt.Order.Market)
        else:
            # 持仓且突破上轨，卖出
            if self.bb.bot < self.datas[0].close:
                self.order = self.close()
            
    def notify_order(self, order):
        # 有交易提交/被接受，啥也不做
        if order.status in [order.Submitted, order.Accepted]:
            return

        self.order = None

if __name__ == '__main__':

    # 加载数据，建立数据源
    start = "2018-01-01"
    end = "2020-07-05"
    name = ["000001.SZ"]
    code = ["000001.SZ"]
    backtest = backtest.BackTest(Bolling, start, end, code, name)
    backtest.run()
    backtest.output()

