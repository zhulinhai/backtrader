import tushare as ts

import os.path  # To manage paths
import sys # To find out the script name (in argv[0])

modpath = os.path.dirname(os.path.abspath(sys.argv[0]))

# append module root directory to sys.path
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

#设置ts令牌
ts.set_token('d1025fd731c0b4b477ef5c4259fcff8ae4aa89a5cd725b54d859a726')

pro = ts.pro_api()

def createFile(filePath):
  if os.path.exists(filePath):
    print('%s:存在'%filePath)
    return True
  else:
    try:
      os.mkdir(filePath)
      print('新建文件夹：%s'%filePath)
      return True
    except Exception as _e:
      os.makedirs(filePath)
      print('新建多层文件夹：%s' % filePath)
      return True
    return False

def save_to_local(code, startdate, enddate):

  #300274为股票代号，直接通过get_hist_data()获取的数据是逆序的，正序数据需要加上sort.index()
  df = pro.daily(ts_code=code, start_date=startdate, end_date=enddate)

  datepath = os.path.join(modpath, 'tushare/{0}'.format(startdate))

  if createFile(datepath):
    datapath = os.path.join(modpath, 'tushare/{0}/{1}.csv'.format(startdate, code))
    #选择需要标签存储股票数据
    df.to_csv(datapath)

save_to_local('000001.SZ', '20180701', '20180718')



#追加数据到现有表
#df.to_sql('tick_data',engine,if_exists='append')