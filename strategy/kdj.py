from __future__ import (absolute_import, division, print_function, unicode_literals)

import datetime # For datetime objects
import os.path  # To manage paths
import sys # To find out the script name (in argv[0])

# append module root directory to sys.path
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# Import the backtrader platform
import backtrader as bt
from backtrader.indicators import EMA


# Create a Strategy
class KdjStrategy(bt.Strategy):
  params = (
    ('maperiod', 20),
    ('exitbars', 5),
  )

  def log(self, txt, dt=None, doprint=True):
    ''' 日志函数，用于统一输出日志格式 '''
    if doprint:
        dt = dt or self.datas[0].datetime.date(0)
        print('%s, %s' % (dt.isoformat(), txt))
  
  def __init__(self):
    # Keep a reference to the "close" line in the data[0] dataseries
    self.dataclose = self.datas[0].close

    # To keep track of pending orders and buy price/commission
    self.order = None
    self.buyprice = None
    self.buycomm = None

    # 9个交易日内最高价
    self.high_nine = bt.indicators.Highest(self.data.high, period=9)
    # 9个交易日内最低价
    self.low_nine = bt.indicators.Lowest(self.data.low, period=9)
    # 计算rsv值
    self.rsv = 100 * bt.DivByZero(
        self.data_close - self.low_nine, self.high_nine - self.low_nine, zero=None
    )
    # 计算rsv的3周期加权平均值，即K值
    self.K = bt.indicators.EMA(self.rsv, period=3, plot=False)
    # D值=K值的3周期加权平均值
    self.D = bt.indicators.EMA(self.K, period=3, plot=False)
    # J=3*K-2*D
    self.J = 3 * self.K - 2 * self.D


  def notify_order(self, order):
    if order.status in [order.Submitted, order.Accepted]:
      # Buy/Sell order submitted/accepted to/by broker - Nothing to do
      return
    # Check if an order has been completed
    # Attention: broker could reject order if not enough cash
    if order.status in [order.Completed]:
      if order.isbuy():
        self.log('BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' % (order.executed.price, order.executed.value, order.executed.comm))
        
        self.buyprice = order.executed.price
        self.buycomm = order.executed.comm


      # 订单因为缺少资金之类的原因被拒绝执行
      elif order.status in [order.Canceled, order.Margin, order.Rejected]:
          self.log('Order Canceled/Margin/Rejected')
        
      else: # Sell
        self.log('SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
          (order.executed.price,
          order.executed.value,
          order.executed.comm))

      self.bar_executed = len(self)

    elif order.status in [order.Canceled, order.Margin, order.Rejected]:
      self.log('Order Canceled/Margin/Rejected')
    
    # Write down: no pending order
    self.order = None

  def notify_trade(self, trade):
    if not trade.isclosed:
      return

    self.log('OPERATION PROFIT, GROSS %.2f, NET %.2f' %
            (trade.pnl, trade.pnlcomm))

  def next(self):
    # Simply log the closing price of the series from the reference
    self.log('Close, %.2f' % self.dataclose[0])

    # Check if an order is pending ... if yes, we cannot send a 2nd one
    if self.order:
      return

    if not self.position:
        # J - D 值
        condition1 = self.J[-1] - self.D[-1]
        condition2 = self.J[0] - self.D[0]
        if condition1 < 0 and condition2 > 0:
            self.log("BUY CREATE, %.2f" % self.dataclose[0])
            self.order = self.buy()

    else:
        condition = (self.dataclose[0] - self.bar_executed_close) / self.dataclose[0]
        if condition > 0.1 or condition < -0.1:
            self.log("SELL CREATE, %.2f" % self.dataclose[0])
            self.order = self.sell()

  def stop(self):
    self.log('Ending Value %.2f' % (self.broker.getvalue()), doprint=True)

if __name__ == '__main__':
  # Create a cerebro entity
  cerebro = bt.Cerebro()

  # Add a strategy
  strats = cerebro.optstrategy(
    KdjStrategy,
    maperiod=range(3, 31))


  modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
  datapath = os.path.join(modpath, '../datas/orcl-1995-2014.txt')

  # Create a Data Feed
  data = bt.feeds.YahooFinanceCSVData(
    dataname=datapath,
    # Do not pass values before the date
    fromdate=datetime.datetime(1995,1,1),
    # Do not pass values after this date
    todate=datetime.datetime(2014,12,31),
    reversed=False
  )

  # Add the Data Feed to Cerebro
  cerebro.adddata(data)
  
  # Set our desired cash start
  cerebro.broker.setcash(10000.0)

  # Add a FixedSize sizer according to the stake
  cerebro.addsizer(bt.sizers.FixedSize, stake=100)

  # 0.1% ... divide by 100 to remove the %
  cerebro.broker.setcommission(commission=0.002)

  print('Starting Profolio Value: %2.f' % cerebro.broker.getvalue())

  # Run over everything
  cerebro.run(maxcpus=1)

  print('Final Profolio Value: %.2f' % cerebro.broker.getvalue())
  cerebro.plot()