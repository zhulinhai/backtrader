from __future__ import (absolute_import, division, print_function, unicode_literals)

import datetime # For datetime objects
import os.path  # To manage paths
import sys # To find out the script name (in argv[0])

# append module root directory to sys.path
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# Import the backtrader platform
import backtrader as bt
from backtrader.indicators import EMA

import datetime
import traceback
import pymysql

from backtrader.feed import DataBase
from backtrader import date2num

from backtrader.analyzers import (SQN, AnnualReturn, TimeReturn, SharpeRatio,
                                  TradeAnalyzer)

class MySQLData(DataBase):
    params = (
        ('fromdate', datetime.datetime.min),
        ('todate', datetime.datetime.max),
        ('ts_code', ''),
    )
    lines = (
        "turnover",
        "turnover_rate"
    )

    def load_data_from_db(self, table, ts_code, start_time, end_time):
        """
        从MySQL加载指定数据
        Args:
            table (str): 表名
            ts_code (str): 股票代码
            start_time (str): 起始时间
            end_time (str): 终止时间
        return:
            data (List): 数据集
        """
        db = pymysql.connect(
            host="localhost",
            user="root",
            password="a1989128",
            db="tushare",
            port=3306
        )

        cur = db.cursor()
        #  WHERE `trade_date` between '{start_time}' and '{end_time}' 
        #  f"and `ts_code` = '{ts_code}' order by `trade_date` asc"
        sql = (
            f"SELECT * FROM `favor_list` WHERE `trade_date` between '{start_time}' and '{end_time}'"
            f"and `ts_code` = '{ts_code}' order by `trade_date` asc"
        )
        cur.execute(sql)
        data = cur.fetchall()
        db.close()
        return iter(list(data))

    def __init__(self, *args, **kwargs):
        self.result = []
        self.empty = False

    def start(self):
        self.result = self.load_data_from_db(self.params.ts_code, self.params.ts_code, self.params.fromdate, self.params.todate)

    def _load(self):
        if self.empty:
            return False
        try:
            one_row = next(self.result)
        except StopIteration:
            return False
        self.lines.datetime[0] = date2num(one_row[2])
        self.lines.open[0] = float(one_row[3])
        self.lines.high[0] = float(one_row[4])
        self.lines.low[0] = float(one_row[5])
        self.lines.close[0] = float(one_row[6])
        self.lines.volume[0] = float(one_row[7])
        self.lines.turnover[0] = float(one_row[8])
        self.lines.turnover_rate[0] = float(one_row[11])
        return True

# Create a Strategy
class MacdKdjStrategy(bt.Strategy):
  params = (
    ('maperiod', 20),
    ('exitbars', 5),
  )

  def log(self, txt, dt=None, doprint=True):
    ''' 日志函数，用于统一输出日志格式 '''
    if doprint:
        dt = dt or self.datas[0].datetime.date(0)
        print('%s, %s' % (dt.isoformat(), txt))
  
  def __init__(self):
    # Keep a reference to the "close" line in the data[0] dataseries
    self.dataclose = self.datas[0].close

    # To keep track of pending orders and buy price/commission
    self.order = None
    self.buyprice = None
    self.buycomm = None

    # 五日移动平均线
    self.sma5 = bt.indicators.SimpleMovingAverage(
        self.datas[0], period=5)
    # 十日移动平均线
    self.sma10 = bt.indicators.SimpleMovingAverage(
        self.datas[0], period=10)

    # 9个交易日内最高价
    self.high_nine = bt.indicators.Highest(self.data.high, period=9)
    # 9个交易日内最低价
    self.low_nine = bt.indicators.Lowest(self.data.low, period=9)
    # 计算rsv值
    self.rsv = 100 * bt.DivByZero(
        self.data_close - self.low_nine, self.high_nine - self.low_nine, zero=None
    )
    # 计算rsv的3周期加权平均值，即K值
    self.K = bt.indicators.EMA(self.rsv, period=3, plot=False)
    # D值=K值的3周期加权平均值
    self.D = bt.indicators.EMA(self.K, period=3, plot=False)
    # J=3*K-2*D
    self.J = 3 * self.K - 2 * self.D
    
    # MACD策略参数
    me1 = EMA(self.data, period=12)
    me2 = EMA(self.data, period=26)
    self.macd = me1 - me2
    self.signal = EMA(self.macd, period=9)


  def notify_order(self, order):
    if order.status in [order.Submitted, order.Accepted]:
      # Buy/Sell order submitted/accepted to/by broker - Nothing to do
      return
    # Check if an order has been completed
    # Attention: broker could reject order if not enough cash
    if order.status in [order.Completed]:
      if order.isbuy():
        self.log('BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' % (order.executed.price, order.executed.value, order.executed.comm))
        
        self.buyprice = order.executed.price
        self.buycomm = order.executed.comm


      # 订单因为缺少资金之类的原因被拒绝执行
      elif order.status in [order.Canceled, order.Margin, order.Rejected]:
          self.log('Order Canceled/Margin/Rejected')
        
      else: # Sell
        self.log('SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
          (order.executed.price,
          order.executed.value,
          order.executed.comm))

      self.bar_executed = len(self)

    elif order.status in [order.Canceled, order.Margin, order.Rejected]:
      self.log('Order Canceled/Margin/Rejected')
    
    # Write down: no pending order
    self.order = None

  def notify_trade(self, trade):
    if not trade.isclosed:
      return

    self.log('OPERATION PROFIT, GROSS %.2f, NET %.2f' %
            (trade.pnl, trade.pnlcomm))

  def next(self):
    # Simply log the closing price of the series from the reference
    self.log('Close, %.2f' % self.dataclose[0])

    # Check if an order is pending ... if yes, we cannot send a 2nd one
    if self.order:
      return
    
    # Check if we are in market
    if not self.position:
      # 如果没有持仓，若前一天MACD < Signal, 当天 Signal < MACD，则第二天买入
      condition1 = self.macd[-1] - self.signal[-1]
      condition2 = self.macd[0] - self.signal[0]
      if condition1 < 0 and condition2 > 0:
          self.log('BUY CREATE, %.2f' % self.dataclose[0])
          self.order = self.buy()

    else:
      # 卖出基于KDJ策略
      condition1 = self.J[-1] - self.D[-1]
      condition2 = self.J[0] - self.D[0]
      if condition1 > 0 or condition2 < 0:
          self.log("SELL CREATE, %.2f" % self.dataclose[0])
          self.order = self.sell()

  def stop(self):
    self.log('Ending Value %.2f' % (self.broker.getvalue()), doprint=True)

if __name__ == '__main__':
  # Create a cerebro entity
  cerebro = bt.Cerebro()

  # Add a strategy
  cerebro.addstrategy(MacdKdjStrategy)

  # Add a strategy
  # strats = cerebro.optstrategy(
  #   KdjStrategy,
  #   maperiod=range(3, 31))

  # 加载数据到模型中
  data = MySQLData(
      ts_code='000989.SZ',
      fromdate=datetime.datetime(2000, 1, 1),
      todate=datetime.datetime(2020, 12, 30),
  )
  # cerebro.adddata(data)

  # modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
  # datapath = os.path.join(modpath, '../datas/XPEV.csv')

  # # Create a Data Feed
  # data = bt.feeds.YahooFinanceCSVData(
  #   dataname=datapath,
  #   # Do not pass values before the date
  #   fromdate=datetime.datetime(2020,8,27),
  #   # Do not pass values after this date
  #   todate=datetime.datetime(2020,12,31),
  #   reversed=False
  # )

  # Add the Data Feed to Cerebro
  cerebro.adddata(data)
  
  # Set our desired cash start
  cerebro.broker.setcash(10000.0)

  # Add a FixedSize sizer according to the stake
  cerebro.addsizer(bt.sizers.FixedSize, stake=10)

  # cerebro.addanalyzer(SharpeRatio, legacyannual=True)

  # 0.1% ... divide by 100 to remove the %
  # cerebro.broker.setcommission(commission=0.002)

  print('Starting Profolio Value: %2.f' % cerebro.broker.getvalue())

  # Run over everything
  cerebro.run(maxcpus=1)

  print('Final Profolio Value: %.2f' % cerebro.broker.getvalue())
  cerebro.plot()