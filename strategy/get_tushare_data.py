from sqlalchemy import create_engine
import pymysql
pymysql.install_as_MySQLdb()

import datetime

import tushare as ts

# append module root directory to sys.path
# sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

#设置ts令牌
ts.set_token('d1025fd731c0b4b477ef5c4259fcff8ae4aa89a5cd725b54d859a726')

pro = ts.pro_api()
engine = create_engine('mysql://root:a1989128@localhost/tushare?charset=utf8')

def query_stock_list():
  #查询当前所有正常上市交易的股票列表
  data = pro.query('stock_basic', exchange='', list_status='L', fields='ts_code,symbol,name,area,industry,list_date')
  return data

def save_stock_basic():
  df = query_stock_list()
  df.to_sql('stock_basic', engine)

def sync_history_data(ts_code, name = '', startdate = ''):
  #多个股票
  # df = pro.daily(ts_code='000001.SZ,600000.SH', start_date='20180701', end_date='20180718')
  df = pro.daily(ts_code=ts_code)

  #存入数据库
  # df.to_sql('tick_data',engine)
  #追加数据到现有表
  df.to_sql(name or 'tushare_data', con=engine, if_exists='append')

def sync_all_data(list = []):
  if (len(list) > 0):
    # 存入数据库
    sync_history_data(','.join(list), 'favor_list')
  else:
    # 获取股票列表
    for ts_code in query_stock_list().ts_code.values:
      # 存入数据库
      sync_history_data(ts_code)


def sync_daily_data():
  # for ts_code in query_stock_list().ts_code.values:
    date_format = datetime.time.strftime('%Y%m%d')
    print(date_format)
    # sync_history_data(ts_code, date_format)

sync_all_data(['000989.SZ', '002083.SZ'])